/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.Controller;

import com.is2t1.app.models.Contact;
import com.is2t1.app.views.ContactFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author Sistemas-15
 */
public class ContactController implements ActionListener , KeyListener{
    private ContactFrame cc;
    private JFileChooser dialog;
    

    public ContactController(ContactFrame cf) {
         cc = cf;
        dialog = new JFileChooser();
       }
    
     public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "save":
                save();
                break;
             case "clear":
                 clear();
                break;
             case "select":
                 select();
                break;               
        }
    }
    
    private void save(){
        dialog.showSaveDialog(cc);
        if(dialog.getSelectedFile()!=null){
          writeFile(dialog.getSelectedFile(), cc.getData());
        }

    }
    private void clear(){
        cc.clear();
    }
    private void select(){
        dialog.showOpenDialog(cc);
        if(dialog.getSelectedFile()!=null){
            Contact c = readFile(dialog.getSelectedFile());
            c.showData(c);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 private void writeFile(File file, Contact cc){
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(cc);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Contact readFile(File file){
        Contact cc = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            cc  = (Contact)in.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cc;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
