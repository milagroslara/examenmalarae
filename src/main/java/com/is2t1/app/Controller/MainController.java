/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.Controller;

import com.is2t1.app.views.ContactFrame;
import com.is2t1.app.views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Sistemas-15
 */
public class MainController implements ActionListener {
    private MainFrame mf;

    public MainController() {
        mf = new MainFrame();
    }
    
    public MainController(MainFrame f){
        mf=f;
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Contactos":
                showContactFrame();
                break; 
        }
     }
    
    
     private void showContactFrame(){
        ContactFrame cm = new ContactFrame();
        mf.desk.add(cm);
        cm.setVisible(true);
    }
    
    
}
